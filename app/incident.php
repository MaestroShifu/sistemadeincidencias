<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class incident extends Model
{
    protected $table = 'incidents';

    protected $fillable = [
        'title','description','severity','category_id','level_id','client_id','support_id',
    ];

    protected $guarded = ['id'];
}
