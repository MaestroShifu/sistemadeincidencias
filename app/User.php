<?php

namespace App;

use App\project;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getIsAdminAttribute(){
        return $this->role == 0;
    }

    public function getIsClientAttribute(){
        return $this->role == 2;
    }

    public function projects()
    {
      return $this->belongsToMany('App\project');
    }

    public function getListOfProjectsAttribute()
    {
      if($this->role == 1){
        return $this->projects;
      }else {
        return project::all();
      }
    }
}
