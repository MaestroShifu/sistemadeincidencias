<?php

namespace App\Http\Controllers\Admin;

use App\category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Es necesario ingresar un nombre para la Categoria'
      ]);

      category::create($request->all());

      return back()->with('notification', 'Ya se a creado una categoria');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Es necesario ingresar un nombre para la Categoria'
      ]);

      $category = category::find($id);
      $category->name = $request->name;
      $category->save();

      return back()->with('notification', 'Ya se a actualizado la categoria');
    }

    public function destroy($id)
    {
      $category = Category::find($id);
      $category->delete();

      return back()->with('notification', 'La categoria se ha deshabilitado correctamente');;
    }

    public function restore($id)
    {
        Category::withTrashed()->find($id)->restore();

        return back()->with('notification', 'La categoria se ha habilitado correctamente');
    }
}
