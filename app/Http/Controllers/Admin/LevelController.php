<?php

namespace App\Http\Controllers\Admin;

use App\level;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{
    public function index()
    {

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Es necesario ingresar un nombre para el nivel'
      ]);

      level::create($request->all());

      return back()->with('notification', 'Ya se a creado un Nivel');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Es necesario ingresar un nombre para el nivel'
      ]);

      $level = level::find($id);
      $level->name = $request->name;
      $level->save();

      return back()->with('notification', 'Ya se a editado el Nivel');
    }

    public function destroy($id)
    {
      $level = Level::find($id);
      $level->delete();

      return back()->with('notification', 'EL Nivel se ha deshabilitado correctamente');;
    }

    public function restore($id)
    {
        Level::withTrashed()->find($id)->restore();

        return back()->with('notification', 'EL Nivel se ha habilitado correctamente');
    }

    public function byProject($id)
    {
      return level::where('project_id', $id)->get();
    }
}
