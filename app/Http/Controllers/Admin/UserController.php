<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\project;
use App\level;
use App\ProjectUser;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        $users = User::withTrashed()->where('role', 1)->get();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ];

        $message = [
            'name.required' => 'Es necesario ingresar el nombre',
            'name.max' => 'No se puede pasar de 255 caracteres',
            'email.required' => 'Email es requerido',
            'email.email' => 'El dato no es un email',
            'email.max' => 'El campo no puede tener mas de 255 caracteres',
            'email.unique' => 'El email ya existe',
            'password.required' => 'Es necesario ingresar contraseña',
            'password.min' => 'La contraseña debe tener como minimo 6 caracteres'
        ];

        $this->validate($request, $rules, $message);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = 1;
        $user->save();

        return back()->with('notification', 'Registro Exitoso');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $user = User::find($id);
        $projects = project::all();
        $projects_user = ProjectUser::where('user_id', $user->id)->get();

        return view('admin.users.edit', compact('user', 'projects', 'projects_user'));
    }

    public function update(Request $request, $id)
    {

        if($request->password){
            $rules = [
                'name' => 'required|max:255',
                'password' => 'min:6'
            ];
        }else{
            $rules = [
                'name' => 'required|max:255',
            ];
        }

        $message = [
            'name.required' => 'Es necesario ingresar el nombre de usuario',
            'name.max' => 'El nombre es demasiado largo',
            'password.min' => 'La contraseña debe tener minimo 6 caracteres'
        ];

        $this->validate($request, $rules, $message);

        $user = User::find($id);

        $user->name = $request->name;
        $password = $request->password;

        if($password){
            $user->password = bcrypt($password);
        }

        $user->save();

        return back();
    }

    public function destroy($id)
    {
        $user = User::find($id);
        //User::destroy($id);
        $user->delete();

        return back();
    }

    public function restore($id)
    {
        User::withTrashed()->find($id)->restore();

        return back()->with('notification', 'EL usuario se ha habilitado correctamente');
    }
}
