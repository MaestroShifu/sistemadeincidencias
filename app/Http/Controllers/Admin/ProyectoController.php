<?php

namespace App\Http\Controllers\Admin;

use App\project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProyectoController extends Controller
{
    public function index()
    {
        $projects = project::withTrashed()->get();

        return view('admin.proyect.index', compact('projects'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, project::$rules, project::$messages);

        project::create($request->all());

        return back()->with('notification', 'EL proyecto se ha registrado correctamente');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $project = project::find($id);
        $categories = $project->categories;
        $levels = $project->levels; //Level::where('project_id', $id)->get();

        return view('admin.proyect.edit', compact('project', 'categories', 'levels'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, project::$rules, project::$messages);

        project::find($id)->update($request->all());

        return back()->with('notification', 'EL proyecto se ha actualizado correctamente');
    }

    public function destroy($id)
    {
        project::find($id)->delete();

        return back()->with('notification', 'EL proyecto se ha deshabilitado correctamente');
    }

    public function restore($id)
    {
        project::withTrashed()->find($id)->restore();

        return back()->with('notification', 'EL proyecto se ha habilitado correctamente');
    }
}
