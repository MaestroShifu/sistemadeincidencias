<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\incident;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getReport () {
        $categories = category::where('project_id', 1)->get();

        return view('report', compact('categories'));
    }

    public function postReport (Request $request) {

        $rules = [
            'title' => 'required|min:5',
            'description' => 'required|min:15',
            'severity' => 'required|in:M,N,A',
            'category_id' => 'exists:categories,id'
        ];

        $message = [
            'category_id.exists' => 'La categoria no existe en la base de datos',
            'title.required' => 'No a ingresado un titulo.',
            'title.min' => 'El titulo debe presentar almenos 5 caracteres',
            'description.required' => 'No a ingresado una descripcion',
            'description.min' => 'La descripcion requiere 15 caracteres como minimo'
        ];

        $this->validate($request, $rules, $message);

        incident::create([
            'title' => $request->title,
            'description' => $request->description,
            'severity' => $request->severity,
            'category_id' => $request->category_id ?:null,
            'client_id' => auth()->user()->id,
        ]);

        return view('home')->with('notification', 'Incidencia registrada satisfactoriamente');
    }
}
