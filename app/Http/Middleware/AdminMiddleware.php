<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! Auth()->check()){
            return redirect('login');
        }

        //vamos a validar si el usuario es admin
        if(auth()->user()->role != 0){
            //no es admin
            return redirect('home');
        }

        return $next($request);
    }
}
