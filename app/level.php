<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class level extends Model
{
    use SoftDeletes;

    protected $table = 'levels';

    protected $fillable = [
        'name', 'project_id',
    ];

    protected $guarded = ['id'];


    public function project()
    {
      //un nivel posee un proyecto
      return $this->belongsTo('App\project');
    }
}
