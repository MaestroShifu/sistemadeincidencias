<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = [
        'name', 'project_id',
    ];

    protected $guarded = ['id'];

    public function project()
    {
      //una categoria posee un proyecto
      return $this->belongsTo('App\project');
    }
}
