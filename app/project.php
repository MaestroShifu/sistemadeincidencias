<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class project extends Model
{
    use SoftDeletes;

    protected $table = 'projects';

    protected $fillable = [
        'name','description','startDate'
    ];

    protected $guarded = ['id'];

    public static $rules = [
        'name' => 'required',
        //'description' => '',
        'startDate' => 'date'
    ];

    public static $messages = [
        'name.required' => 'Esnecesario ingresar un nombre para el proyecto',
        'startDate.date' => 'La fecha no tiene un formato adecuado'
    ];

    public function categories()
    {
      //un proyecto posee muchas categorias
      return $this->hasMany('App\category')->withTrashed();
    }

    public function levels()
    {
      //un proyecto posee muchas niveles
      return $this->hasMany('App\level')->withTrashed();
    }

    public function users()
    {
      return $this->belongsToMany('App\User');
    }
}
