<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
    protected $table = 'project_user';

    protected $fillable = [
        'user_id','project_id','level_id'
    ];

    public function project()
    {
      return $this->belongsTo('App\project');
    }

    public function level()
    {
      return $this->belongsTo('App\Level');
    }
}
