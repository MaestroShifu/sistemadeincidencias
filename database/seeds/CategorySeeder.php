<?php

use Illuminate\Database\Seeder;
use App\category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        category::create([
            'name' => 'Categoria A1',
            'project_id' => '1',
        ]);
        category::create([
            'name' => 'Categoria A2',
            'project_id' => '1',
        ]);
        category::create([
            'name' => 'Categoria B1',
            'project_id' => '2',
        ]);
        category::create([
            'name' => 'Categoria B2',
            'project_id' => '2',
        ]);
    }
}
