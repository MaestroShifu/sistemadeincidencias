<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com', 
            'password' => bcrypt('123456789'),
            'role' => '0'
        ]);

        //Support
        User::create([
            'name' => 'Support',
            'email' => 'supp@supp.com', 
            'password' => bcrypt('123456789'),
            'role' => '1'
        ]);

        //User
        User::create([
            'name' => 'User',
            'email' => 'user@user.com', 
            'password' => bcrypt('123456789'),
            'role' => '2'
        ]);
    }
}
