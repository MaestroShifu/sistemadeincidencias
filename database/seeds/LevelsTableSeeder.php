<?php

use Illuminate\Database\Seeder;
use App\level;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        level::create([
          'name' => 'Atencion por telefono',
          'project_id' => 1
        ]);
        level::create([
          'name' => 'Envio de tecnico',
          'project_id' => 1
        ]);
        level::create([
          'name' => 'Mesa de ayuda',
          'project_id' => 2
        ]);
        level::create([
          'name' => 'Consulta especializada',
          'project_id' => 2
        ]);
    }
}
