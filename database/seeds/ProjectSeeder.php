<?php

use Illuminate\Database\Seeder;
use App\project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        project::create([
            'name' => 'Proyectos A',
            'description' => 'Consiste en desarrollar una web',
            'startDate' => '1997-08-19'
        ]);

        project::create([
            'name' => 'Proyectos B',
            'description' => 'Consiste en desarrollar una Aplicacion en android',
            'startDate' => '1997-08-20'
        ]);
    }
}
