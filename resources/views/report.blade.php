@extends('layouts.app')

@section('content')

            <!-- Formulario Registrar Incidencias -->
            <div class="card">
                <div class="card-header">Registrar Incidencia</div>
                <div class="card-body">

                    @include('messageError')
                    @include('messageSuccess')

                    <form action="{{route('createReport')}}" method="POST">

                        <!-- token de seguridad -->
                        {{ csrf_field() }}

                        <!-- Categoria -->
                        <div class="form-group">
                            <label for="category_id">Categoria</label>
                            <select name="category_id" class="form-control">
                                @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- Severidad -->
                        <div class="form-group">
                            <label for="severity">Severidad</label>
                            <select name="severity" class="form-control">
                                <option value="M">Menor</option>
                                <option value="N">Normal</option>
                                <option value="A">Alta</option>
                            </select>
                        </div>

                        <!-- Titulo -->
                        <div class="form-group">
                            <label for="title">Titulo</label>
                            <input type="text" class="form-control" name="title" value="{{old('title')}}">
                        </div>

                        <!-- Descripcion -->
                        <div class="form-group">
                            <label for="description">Descripcion</label>
                            <textarea name="description" class="form-control">{{old('description')}}</textarea>
                        </div>

                        <!-- Boton Agregar -->
                        <div class="form-group">
                            <button class="btn btn-primary">Registrar Incidencias</button>
                        </div>
                    </form>

                </div>
            </div>

@endsection
