<div class="card">

        <div class="card-header">Menu</div>
        <div class="card-body">

            <div class="list-group">
                @if(auth()->check())

                    <a href="/home" class="list-group-item list-group-item-action">
                        Dashboard
                    </a>

                    @if(! auth()->user()->is_client)
                        <a href="#" class="list-group-item list-group-item-action">
                            Ver incidencias
                        </a>
                    @endif
                    <a href="/reportar" class="list-group-item list-group-item-action">
                        Reportar incidencias
                    </a>

                    <!-- DropDow de menu -->

                    @if(auth()->user()->is_admin)
                        <a class="list-group-item list-group-item-action dropdown dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Administracion</a>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="{{route('usuarios.index')}}">Usuarios</a>
                        <a class="dropdown-item" href="{{route('proyectos.index')}}">Proyectos</a>
                        <a class="dropdown-item" href="/config">Configuracion</a>
                        </div>
                    @endif
                      


                @else

                    <a href="/" class="list-group-item list-group-item-action">
                        Bienvenido
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Instrucciones
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        creditos
                    </a>

                @endif
            </div>

        </div>
</div>