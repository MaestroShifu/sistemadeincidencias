@extends('layouts.app')

@section('content')

        <!-- Formulario de login -->
        <div class="card">
        <h5 class="card-header">Login</h5>
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

                <!--Campo Email -->
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                </div>

                <!-- Campo Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>

                <!-- Recordar Usuario  -->
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox"  name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                    </label>
                </div>
                <br>

                <!-- Login -->
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>

@endsection
