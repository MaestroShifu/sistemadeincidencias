@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">Registrar Usuario</div>
    <div class="card-body">
        @include('messageSuccess')
        @include('messageError')
        <form action="{{route('usuarios.update', $user->id)}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH')}}


            <!-- Name -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $user->name )}}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email',  $user->email) }}" readonly required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>

            <!-- Password -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Password <em>Si deseas cambiar la clave</em></label>
                <input id="password" type="text" class="form-control" name="password" value="{{ old('password')}}">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>

        </form>

        <!-- sleect proyecto y nivel mas boton asignar proyecto -->
        <form action="{{route('proyecto.usuario')}}" method="post">
          {{ csrf_field() }}
        <div class="row">
            <input type="hidden" name="user_id" value="{{$user->id}}">
          <div class="col-md-4">
            <div class="form-group">
                <select name="project_id" class="form-control" id="select-project">
                    <option selected="">Seleccione proyecto</option>
                    @foreach ($projects as $key => $project)
                      <option value="{{$project->id}}">{{$project->name}}</option>
                    @endforeach
                </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <select name="level_id" class="form-control" id="select-level">
                    <option selected="">Seleccione nivel</option>
                </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Asignar proyecto</button>
            </div>
          </div>
        </div>
        </form>

        <div class="card">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"> # </th>
                        <th scope="col">Proyecto</th>
                        <th scope="col">Nivel</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($projects_user as $key => $project_user)
                    <tr class="table-active">
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$project_user->project->name}}</td>
                        <td>{{$project_user->level->name}}</td>
                        <td>
                            <form action="{{route('proyecto.usuario.delete', $project_user->id)}}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('DELETE')}}
                              <button  class="btn btn-sm btn-danger" title="Eliminar" type="submit" name="button"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
