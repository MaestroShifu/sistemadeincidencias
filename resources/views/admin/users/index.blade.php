@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">Registrar Usuario</div>
    <div class="card-body">
        @include('messageError')
        @include('messageSuccess')

        <form action="{{route('usuarios.store')}}" method="POST">
            {{ csrf_field() }}

            <!-- Name -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
            </div>

            <!-- Email -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>

            <!-- Password -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Password</label>
                <input id="password" type="text" class="form-control" name="password" value="{{ old('password', str_random(8)) }}" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>

            <!-- Boton Agregar -->
            <div class="form-group">
                <button class="btn btn-primary">Registrar</button>
            </div>
        </form>

        <div class="card">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"> Id </th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr class="table-active">
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->email}}</td>
                        <td>{{$user->name}}</td>
                        <td>

                            <form action="{{route('usuarios.destroy', $user->id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE')}}

                                @if($user->trashed())
                                    <a class="btn btn-sm btn-info" title="Restaurar" href="{{route('usuarios.restore', $user->id)}}"><i class="fas fa-undo"></i></a>
                                @else
                                    <a class="btn btn-sm btn-primary" title="Editar" href="{{route('usuarios.edit', $user->id)}}"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-sm btn-danger" title="Eliminar" ><i class="fas fa-trash-alt"></i></button>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
