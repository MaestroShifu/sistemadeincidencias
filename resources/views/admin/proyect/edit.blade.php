@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">Registrar Proyecto</div>
    <div class="card-body">

        @include('messageError')
        @include('messageSuccess')
        <form action="{{route('proyectos.update', $project->id)}}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PATCH')}}

            <!-- Name -->
            <div class="form-group">
                <label for="name">Nombre</label>
                <input id="name" type="text" class="form-control" value="{{old('name', $project->name)}}" name="name" required autofocus>
            </div>

            <!-- description -->
            <div class="form-group">
                <label for="description">Descripcion</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{$project->description}}</textarea>
            </div>

            <!-- date Start -->
            <div class="form-group">
                <label for="startDate">Fecha de Inicio</label>
                <input id="startDate" type="date" class="form-control" value="{{old('startDate', $project->startDate)}}" name="startDate">
            </div>

            <!-- Boton Agregar -->
            <div class="form-group">
                <button class="btn btn-primary">Guardar Proyecto</button>
            </div>
        </form>


        <div class="row">
            <div class="col-md-6">
                <!-- CAtegorias -->
                <span>Categorias</span>
                <form action="{{route('categorias.store')}}" method="POST" class="form-inline">
                  {{ csrf_field() }}
                  <input type="hidden" name="project_id" value="{{$project->id}}">
                  <div class="form-group">
                      <input type="text" name="name" placeholder="Ingrese Nombre" class="form-control btn-sm">
                  </div>
                  <button class="btn btn-sm btn-success form-control" type="submit">Añadir</button>
                </form>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($categories as $key => $category)
                        <tr class="table-active">
                            <td>{{$category->name}}</td>
                            <td>
                              <form action="{{route('categorias.destroy', $category->id)}}" method="POST">
                                  {{ csrf_field() }}
                                  {{ method_field('DELETE')}}

                                  @if($category->trashed())
                                      <a class="btn btn-sm btn-info" title="Restaurar" href="{{route('categorias.restore', $category->id)}}"><i class="fas fa-undo"></i></a>
                                  @else
                                      <button type="button" class="btn btn-sm btn-primary" title="Editar" data-toggle="modal" data-target="#editCategory{{$category->id}}"><i class="fas fa-edit"></i></button>
                                      <button class="btn btn-sm btn-danger" title="Eliminar" ><i class="fas fa-trash-alt"></i></button>
                                  @endif
                              </form>
                            </td>
                        </tr>

                        <!-- Modal edit Category -->
                        <div class="modal fade" id="editCategory{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              @include('messageError')
                              <form class="" action="{{route('categorias.update', $category->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH')}}
                                <div class="modal-body">
                                  <div class="form-group">
                                      <label for="name">Nombre de la categoria</label>
                                      <input id="name" type="text" class="form-control" value="{{old('name', $category->name)}}" name="name" required autofocus>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                  <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <!-- NIveles -->
                <span>Niveles</span>
                <form action="{{route('levels.store')}}" method="POST" class="form-inline">
                  {{ csrf_field() }}

                  <input type="hidden" name="project_id" value="{{$project->id}}">
                  <div class="form-group">
                      <input type="text" name="name" placeholder="Ingrese Nombre" class="form-control btn-sm">
                  </div>
                  <button class="btn btn-sm btn-success form-control" type="submit">Añadir</button>
                </form>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"> # </th>
                            <th scope="col">Nivel</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($levels as $key => $level)
                        <tr class="table-active">
                            <th scope="row">N{{$key+1}}</th>
                            <td>{{$level->name}}</td>
                            <td>
                              <form action="{{route('levels.destroy', $level->id)}}" method="POST">
                                  {{ csrf_field() }}
                                  {{ method_field('DELETE')}}

                                  @if($level->trashed())
                                      <a class="btn btn-sm btn-info" title="Restaurar" href="{{route('levels.restore', $level->id)}}"><i class="fas fa-undo"></i></a>
                                  @else
                                      <button type="button" class="btn btn-sm btn-primary" title="Editar" data-toggle="modal" data-target="#editLevel{{$level->id}}"><i class="fas fa-edit"></i></button>
                                      <button class="btn btn-sm btn-danger" title="Eliminar" ><i class="fas fa-trash-alt"></i></button>
                                  @endif
                              </form>
                            </td>
                        </tr>

                        <!-- Modal edit level -->
                        <div class="modal fade" id="editLevel{{$level->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              @include('messageError')
                              <form class="" action="{{route('levels.update', $level->id)}}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('PATCH')}}
                                <div class="modal-body">
                                  <div class="form-group">
                                      <label for="name">Nombre del nivel</label>
                                      <input id="name" type="text" class="form-control" value="{{old('name', $level->name)}}" name="name" required autofocus>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                  <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
