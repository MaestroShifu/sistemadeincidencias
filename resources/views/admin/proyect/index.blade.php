@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-header">Registrar Proyectos</div>
    <div class="card-body">
        @include('messageError')
        @include('messageSuccess')

        <form action="{{route('proyectos.store')}}" method="POST">
            {{ csrf_field() }}

            <!-- Name -->
            <div class="form-group">
                <label for="name">Nombre</label>
                <input id="name" type="text" class="form-control" value="{{old('name')}}" name="name" required>
            </div>

            <!-- description -->
            <div class="form-group">
                <label for="description">Descripcion</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{old('description')}}</textarea>
            </div>

            <!-- date Start -->
            <div class="form-group">
                <label for="startDate">Fecha de Inicio</label>
                <input id="startDate" type="date" class="form-control" value="{{old('startDate')}}" name="startDate">
            </div>

            <!-- Boton Agregar -->
            <div class="form-group">
                <button class="btn btn-primary">Registrar Proyecto</button>
            </div>
        </form>

        <div class="card">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"> Id </th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Fecha de Inicio</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                    <tr class="table-active">
                        <th scope="row">{{$project->id}}</th>
                        <td>{{$project->name}}</td>
                        <td>{{$project->description}}</td>
                        <td>{{$project->startDate ?: 'No se ha indicado'}}</td>
                        <td>

                            <form action="{{route('proyectos.destroy', $project->id)}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE')}}

                                @if($project->trashed())
                                    <a class="btn btn-sm btn-info" title="Restaurar" href="{{route('proyectos.restore', $project->id)}}"><i class="fas fa-undo"></i></a>
                                @else
                                    <a class="btn btn-sm btn-primary" title="Editar" href="{{route('proyectos.edit', $project->id)}}"><i class="fas fa-edit"></i></a>
                                    <button class="btn btn-sm btn-danger" title="Eliminar" ><i class="fas fa-trash-alt"></i></button>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
