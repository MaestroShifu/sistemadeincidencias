<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/reportar', 'HomeController@postReport')->name('createReport');
Route::get('/reportar', 'HomeController@getReport');

Route::group(['middleware' => 'admin', 'namespace' => 'Admin'], function() {
    // Usuarios
    Route::resource('/usuarios', 'UserController');
    Route::get('/usuarios/{usuarios}/restaurar', 'UserController@restore')->name('usuarios.restore');
    //Proyectos
    Route::resource('/proyectos', 'ProyectoController');
    Route::get('/proyectos/{proyecto}/restaurar', 'ProyectoController@restore')->name('proyectos.restore');
    //Categoria
    Route::resource('/categorias', 'CategoryController');
    Route::get('/categorias/{categoria}/restaurar', 'CategoryController@restore')->name('categorias.restore');
    //Level
    Route::resource('/levels', 'LevelController');
    Route::get('/levels/{level}/restaurar', 'LevelController@restore')->name('levels.restore');

    //Project UserController
    Route::post('/proyecto-usuario', 'ProjectUserController@store')->name('proyecto.usuario');
    Route::delete('/proyecto-usuario/{id}/eliminar', 'ProjectUserController@delete')->name('proyecto.usuario.delete');

    Route::get('/config', 'ConfigController@index');
});
